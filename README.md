# OPTI-SYSTEMD

- opti-systemd-version: sep 2023

- build-latest: 0.0.1

- Support for the distro: Ubuntu/Debian/Arch/Manjaro

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- Use at your own risk

- opti-systemd is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with opti-systemd, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of opti-systemd to add additional features.

## Installation

- To install OPTI-SYSTEMD, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/opti-systemd.git

# 2. To install the OPTI-SYSTEMD script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# For check version

sudo `opti-systemd-version` or `opti-systemd-version`

# For stop the opti-systemd

sudo `opti-systemd-stop`

# For start the opti-systemd

sudo `opti-systemd-start`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# If opti-systemd causes the system to fail to start, use the following command:

- It backs up the "systemd.conf" file with the name "systemd.conf.bak" in "/etc/systemd".
- If any issues arise, you can simply delete the "systemd.conf" file and use the following command to copy the backup "systemd.conf.bak" back to "/etc/systemd":

- To begin, start from a live CD of your distribution, which can be Ubuntu, Lubuntu, Arch Linux, or another of your choice.

- You will need to mount the partition where the '/' (slash) is located, and you should mount it at /mnt.

- sudo rm -rf /mnt/etc/systemd/systemd.conf

- sudo cp /mnt/etc/systemd/systemd.conf.bak /etc/systemd/systemd.conf

- When the system starts, you download 'opti-systemd,' and in the folder, to uninstall, use the following command:

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

- If any issues occur, you can address them using this uninstallation process.

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The OPTI-SYSTEMD project is currently in development. The latest stable version is 0.0.1. We aim to provide regular updates and add more features in the future.

# License

- OPTI-SYSTEMD is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of OPTI-SYSTEMD.
